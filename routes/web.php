<?php

use App\Http\Controllers\backendController;
use App\Http\Controllers\BookTableController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ChefController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\frontendController;
use App\Http\Controllers\ItemController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\user_typeController;
use App\Http\Controllers\SizeController;
use App\Http\Controllers\userController;
use App\Models\user_type;
use Illuminate\Support\Facades\Route;
use Symfony\Component\Translation\CatalogueMetadataAwareInterface;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('frontend.home');
});


Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__.'/auth.php';
// backend
Route::get('/admin', [backendController::class, 'dashboard'])->middleware('auth');
Route::get('/search', [backendController::class, 'search'])->name('search')->middleware('auth');
// fronteend
Route::get('/', [frontendController::class, 'home'])->name('front_home');
Route::get('/redirects', [frontendController::class, 'redirects'])->name('redirects');
Route::get('/about', [frontendController::class, 'about'])->name('front_about');
Route::get('/service', [frontendController::class, 'service'])->name('front_service');
Route::get('/menu', [frontendController::class, 'menu'])->name('front_menu');
Route::get('/team', [frontendController::class, 'team'])->name('front_team');
Route::get('/contact', [frontendController::class, 'contact'])->name('front_contact');
Route::get('/categoryWiseItem/{id}', [frontendController::class, 'categoryWiseItem'])->name('front_categoryWiseItem');
Route::get('/cart/{id}', [frontendController::class, 'showcart'])->name('front_cart');
Route::get('/cart_item/{id}', [frontendController::class, 'delete'])->name('cart_item_delete');
Route::get('/order', [frontendController::class, 'order'])->name('front_order');
Route::get('/item_details/{id}', [frontendController::class, 'item_details'])->name('item_details');
Route::get('/table_booking', [frontendController::class, 'tablebooking'])->name('front_tble_booking');

Route::prefix('Categories')->group(function(){
    // index
    Route::get('/index', [CategoryController::class, 'index'])->name('category_index')->middleware('auth');
    // show
    Route::get('/show/{id}', [CategoryController::class, 'show'])->name('category_show')->middleware('auth');
    // create
    Route::get('/create', [CategoryController::class, 'create'])->name('category_create')->middleware('auth');
     // insert
     Route::post('/insert', [CategoryController::class, 'insert'])->name('category_insert')->middleware('auth');
    // edit
    Route::get('/edit/{id}', [CategoryController::class, 'edit'])->name('category_edit')->middleware('auth');
    // update
    Route::post('/update/{id}', [CategoryController::class, 'update'])->name('category_update')->middleware('auth');
     // delete
     Route::get('/delete/{id}', [CategoryController::class, 'delete'])->name('category_delete')->middleware('auth');
      // trashlist
    Route::get('/trash-list', [CategoryController::class, 'trashlist'])->name('category_trashlist')->middleware('auth');
    // restore
    Route::get('/restore/{id}', [CategoryController::class, 'restore'])->name('category_restore')->middleware('auth');
});

Route::prefix('Carts')->group(function(){
     // index
     Route::post('/addcart/{id}', [CartController::class, 'addcart'])->name('addcart')->middleware('auth');
    // index
    Route::get('/index', [CartController::class, 'index'])->name('cart_index')->middleware('auth');
    // show
    Route::get('/show/{id}', [CartController::class, 'show'])->name('cart_show')->middleware('auth');
    // create
    Route::get('/create', [CartController::class, 'create'])->name('cart_create')->middleware('auth');
     // insert
     Route::post('/insert', [CartController::class, 'insert'])->name('cart_insert')->middleware('auth');
    // edit
    Route::get('/edit/{id}', [CartController::class, 'edit'])->name('cart_edit')->middleware('auth');
    // update
    Route::post('/update/{id}', [CartController::class, 'update'])->name('cart_update')->middleware('auth');
     // delete
     Route::get('/delete/{id}', [CartController::class, 'delete'])->name('cart_delete')->middleware('auth');
      // trashlist
    Route::get('/trash-list', [CartController::class, 'trashlist'])->name('cart_trashlist')->middleware('auth');
    // restore
    Route::get('/restore/{id}', [CartController::class, 'restore'])->name('cart_restore')->middleware('auth');
});

Route::prefix('Comments')->group(function(){
    // index
    Route::get('/index', [CommentController::class, 'index'])->name('comment_index')->middleware('auth');
    // show
    Route::get('/show/{id}', [CommentController::class, 'show'])->name('comment_show')->middleware('auth');
    // create
    Route::get('/create', [CommentController::class, 'create'])->name('comment_create')->middleware('auth');
     // insert
     Route::post('/insert', [CommentController::class, 'insert'])->name('comment_insert')->middleware('auth');
    // edit
    Route::get('/edit/{id}', [CommentController::class, 'edit'])->name('comment_edit')->middleware('auth');
    // update
    Route::post('/update/{id}', [CommentController::class, 'update'])->name('comment_update')->middleware('auth');
     // delete
     Route::get('/delete/{id}', [CommentController::class, 'delete'])->name('comment_delete')->middleware('auth');
      // trashlist
    Route::get('/trash-list', [CommentController::class, 'trashlist'])->name('comment_trashlist')->middleware('auth');
    // restore
    Route::get('/restore/{id}', [CommentController::class, 'restore'])->name('comment_restore')->middleware('auth');
});

Route::prefix('Orders')->group(function(){
    // index
    Route::get('/index', [OrderController::class, 'index'])->name('order_index')->middleware('auth');
    // show
    Route::get('/show/{id}', [OrderController::class, 'show'])->name('order_show')->middleware('auth');
    // create
    Route::get('/create', [OrderController::class, 'create'])->name('order_create')->middleware('auth');
     // insert
     Route::post('/insert/{id}', [OrderController::class, 'insert'])->name('order_insert')->middleware('auth');
    // edit
    Route::get('/edit/{id}', [OrderController::class, 'edit'])->name('order_edit')->middleware('auth');
    // update
    Route::post('/update/{id}', [OrderController::class, 'update'])->name('order_update')->middleware('auth');
     // delete
     Route::get('/delete/{id}', [OrderController::class, 'delete'])->name('order_delete')->middleware('auth');
      // trashlist
    Route::get('/trash-list', [OrderController::class, 'trashlist'])->name('order_trashlist')->middleware('auth');
    // restore
    Route::get('/restore/{id}', [OrderController::class, 'restore'])->name('order_restore')->middleware('auth');
});

Route::prefix('User_type')->group(function(){
    // index
    Route::get('/index', [user_type::class, 'index'])->name('user_type_index')->middleware('auth');
    // show
    Route::get('/show/{id}', [user_type::class, 'show'])->name('user_type_show')->middleware('auth');
    // create
    Route::get('/create', [user_type::class, 'create'])->name('user_type_create')->middleware('auth');
     // insert
     Route::post('/insert', [user_type::class, 'insert'])->name('user_type_insert')->middleware('auth');
    // edit
    Route::get('/edit/{id}', [user_type::class, 'edit'])->name('user_type_edit')->middleware('auth');
    // update
    Route::post('/update/{id}', [user_type::class, 'update'])->name('user_type_update')->middleware('auth');
     // delete
     Route::get('/delete/{id}', [user_type::class, 'delete'])->name('user_type_delete')->middleware('auth');
      // trashlist
    Route::get('/trash-list', [user_type::class, 'trashlist'])->name('user_type_trashlist')->middleware('auth');
    // restore
    Route::get('/restore/{id}', [user_type::class, 'restore'])->name('user_type_restore')->middleware('auth');
});

Route::prefix('Table Booking ')->group(function(){
    // index
    Route::get('/index', [BookTableController::class, 'index'])->name('t_booking_index')->middleware('auth');
    // show
    Route::get('/show/{id}', [BookTableController::class, 'show'])->name('t_booking_show')->middleware('auth');
    // create
    Route::get('/create', [BookTableController::class, 'create'])->name('t_booking_create')->middleware('auth');
     // insert
     Route::post('/insert', [BookTableController::class, 'insert'])->name('insert')->middleware('auth');
    // edit
    Route::get('/edit/{id}', [BookTableController::class, 'edit'])->name('t_booking_edit')->middleware('auth');
    // update
    Route::post('/update/{id}', [BookTableController::class, 'update'])->name('t_booking_update')->middleware('auth');
     // delete
     Route::get('/delete/{id}', [BookTableController::class, 'delete'])->name('t_booking_delete')->middleware('auth');
      // trashlist
    Route::get('/trash-list', [BookTableController::class, 'trashlist'])->name('t_booking_trashlist')->middleware('auth');
    // restore
    Route::get('/restore/{id}', [BookTableController::class, 'restore'])->name('t_booking_restore')->middleware('auth');
});

Route::prefix('item ')->group(function(){
    // index
    Route::get('/index', [ItemController::class, 'index'])->name('item_index')->middleware('auth');
    // show
    Route::get('/show/{id}', [ItemController::class, 'show'])->name('item_show')->middleware('auth');
    // create
    Route::get('/create', [ItemController::class, 'create'])->name('item_create')->middleware('auth');
     // insert
     Route::post('/insert', [ItemController::class, 'insert'])->name('item_insert')->middleware('auth');
    // edit
    Route::get('/edit/{id}', [ItemController::class, 'edit'])->name('item_edit')->middleware('auth');
    // update
    Route::post('/update/{id}', [ItemController::class, 'update'])->name('item_update')->middleware('auth');
     // delete
     Route::get('/delete/{id}', [ItemController::class, 'delete'])->name('item_delete')->middleware('auth');
      // trashlist
    Route::get('/trash-list', [ItemController::class, 'trashlist'])->name('item_trashlist')->middleware('auth');
    // restore
    Route::get('/restore/{id}', [ItemController::class, 'restore'])->name('item_restore')->middleware('auth');
});

Route::prefix('Size ')->group(function(){
    // index
    Route::get('/index', [SizeController::class, 'index'])->name('size_index')->middleware('auth');
    // show
    Route::get('/show/{id}', [SizeControlle::class, 'show'])->name('size_show')->middleware('auth');
    // create
    Route::get('/create', [SizeControlle::class, 'create'])->name('size_create')->middleware('auth');
     // insert
     Route::post('/insert', [SizeControlle::class, 'insert'])->name('size_insert')->middleware('auth');
    // edit
    Route::get('/edit/{id}', [SizeControlle::class, 'edit'])->name('size_edit')->middleware('auth');
    // update
    Route::post('/update/{id}', [SizeControlle::class, 'update'])->name('size_update')->middleware('auth');
     // delete
     Route::get('/delete/{id}', [SizeControlle::class, 'delete'])->name('size_delete')->middleware('auth');
      // trashlist
    Route::get('/trash-list', [SizeControlle::class, 'trashlist'])->name('size_trashlist')->middleware('auth');
    // restore
    Route::get('/restore/{id}', [SizeControlle::class, 'restore'])->name('size_restore')->middleware('auth');
});

Route::prefix('user ')->group(function(){
    // index
    Route::get('/index', [userController::class, 'index'])->name('user_index')->middleware('auth');
    // show
    Route::get('/show/{id}', [userController::class, 'show'])->name('user_show')->middleware('auth');
    // create
    Route::get('/create', [userController::class, 'create'])->name('user_create')->middleware('auth');
     // insert
     Route::post('/insert', [userController::class, 'insert'])->name('user_insert')->middleware('auth');
    // edit
    Route::get('/edit/{id}', [userController::class, 'edit'])->name('user_edit')->middleware('auth');
    // update
    Route::post('/update/{id}', [userController::class, 'update'])->name('user_update')->middleware('auth');
     // delete
     Route::get('/delete/{id}', [userController::class, 'delete'])->name('user_delete')->middleware('auth');
      // trashlist
    Route::get('/trash-list', [userController::class, 'trashlist'])->name('user_trashlist')->middleware('auth');
    // restore
    Route::get('/restore/{id}', [userController::class, 'restore'])->name('user_restore')->middleware('auth');
});

Route::prefix('chefs ')->group(function(){
    // index
    Route::get('/index', [ChefController::class, 'index'])->name('chefs_index')->middleware('auth');
    // show
    Route::get('/show/{id}', [ChefController::class, 'show'])->name('chefs_show')->middleware('auth');
    // create
    Route::get('/create', [ChefController::class, 'create'])->name('chefs_create')->middleware('auth');
     // insert
     Route::post('/insert', [ChefController::class, 'insert'])->name('chefs_insert')->middleware('auth');
    // edit
    Route::get('/edit/{id}', [ChefController::class, 'edit'])->name('chefs_edit')->middleware('auth');
    // update
    Route::post('/update/{id}', [ChefController::class, 'update'])->name('chefs_update')->middleware('auth');
     // delete
     Route::get('/delete/{id}', [ChefController::class, 'delete'])->name('chefs_delete')->middleware('auth');
      // trashlist
    Route::get('/trash-list', [ChefController::class, 'trashlist'])->name('chefs_trashlist')->middleware('auth');
    // restore
    Route::get('/restore/{id}', [ChefController::class, 'restore'])->name('chefs_restore')->middleware('auth');
});