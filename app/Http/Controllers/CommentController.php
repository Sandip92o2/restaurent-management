<?php

namespace App\Http\Controllers;

use Exception;
use App\Models\User;
use App\Models\Comment;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function index()
    {
        $comments=Comment::all();

        $deleteItem=Comment::onlyTrashed()->count();
        return view('backend.comments.index', compact('comments', 'deleteItem'));
    }

    public function show($id)
    {
        $comment=Comment::find($id);
        return view('backend.comments.show', compact('comment'));
    }

    public function create()
    {
        return view('backend.comments.create');
    }

    public function insert(Request $request)
    {
        try{
            $data=$request->all();

            Comment::create($data);



            return redirect()->route('comment_index')->withMessage('Inserted Success');
        }catch(Exception $e){
            dd($e->getMessage());
        }
    }

    public function edit($id)
    {
        $comment=Comment::find($id);
        return view('backend.comments.edit', compact('comment'));
    }

    public function update(Request $request, $id)
    {
        try{
            $data=$request->except('_token');
            Comment::where('id', $id)->update($data);
            return redirect()->route('comment_index')->withMessage('Updated Success');
        }catch(Exception $e){
            dd($e->getMessage());
        }
    }

    public function delete($id)
    {
        $comment=Comment::find($id);
        $comment->delete();
        return redirect()->back()->withMessage('Deleted Success');
    }

    public function trashlist()
    {
        $comments=Comment::onlyTrashed()->get();
        return view('backend.comments.trashlist', compact('comments'));
    }

    public function restore($id)
    {
        Comment::withTrashed()->where('id', $id)->restore();
        return redirect()->route('comment_index');
    }

}
