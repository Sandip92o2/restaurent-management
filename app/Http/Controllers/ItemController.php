<?php

namespace App\Http\Controllers;

use Image;
use Exception;
use App\Models\Item;
use App\Models\Category;
use Mockery\Expectation;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class ItemController extends Controller
{
    public function index()
    {
        $items=Item::all();
        $deleteItem=Item::onlyTrashed()->count();
        return view('backend.items.index', compact('items', 'deleteItem'));
        
    }

    
    public function show($id)
    {
        $item=Item::find($id);
        return view('backend.items.show', compact('item'));
    }

    public function create()
    {
        $categories=Category::all();
        
        return view('backend.items.create', compact('categories'));
    }

    public function insert(Request $request)
    {
      try{
        $data=$request->all();
        if($request->image){
            $image =$this->uploadedImage($request->name, $request->image);
        }
        
        $data['image'] = $image;
       
        Item::create($data);

        return redirect()->route('item_index')->withMessage('Inserted Success');
      }catch(Expectation $e){
        dd($e->getExceptionMessage());
      }

    }

    public function edit($id)
    {
        $item=Item::find($id);
        return view('backend.items.edit', compact('item'));
    }


    public function Update(Request $request, $id)
    {   
        try{
            // data collect
            $data=$request->except('_token');
            // image update
            if($request->hasFile('image')){
                $item = Item::where('id', $id)->first();

                $this->unlink($item->image);

                $data['image'] = $this->uploadedImage($request->name, $request->image);
            } 
            // data insert
            item::where('id', $id)->Update($data);
            // move to index page
            return redirect()->route('item_index')->withMessage('Updated Success');
        }catch(Exception $e){
            
          dd($e->getMessage());
        }
    }

    public function delete($id)
    {
        $item=Item::find($id);
        $item->delete();
        return redirect()->back()->withMessage('Deleted Success');
    }

    public function trashlist()
    {
        $items=Item::onlyTrashed()->get();
        return view('backend.items.trashlist', compact('items'));
    }

    public function restore($id)
    {
        Item::withTrashed()->where('id', $id)->restore();
        return redirect()->route('item_index');

    }

    public function uploadedImage($name, $image)
    {
        //name ba title er nam uniq korar jonno  
        $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
        
        $file_name = $timestamp .'-'.$name. '.' . $image->getClientOriginalExtension();
        
        // storage e photo 
        $pathToUpload = storage_path().'/app/public/items/';  // image  upload application save korbo
        // automatic folader create...
        if(!is_dir($pathToUpload)) {

            mkdir($pathToUpload, 0755, true);

        }

        Image::make($image)->resize(634,792)->save($pathToUpload.$file_name);

        return $file_name;
    }

    
    private function unlink($image)
    {
        $pathToUpload = storage_path().'/app/public/items/';
        if ($image != '' && file_exists($pathToUpload. $image)) {
            @unlink($pathToUpload. $image);
        }

    }

}






