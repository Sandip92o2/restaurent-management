<?php

namespace App\Http\Controllers;

use App\Models\size;
use Illuminate\Http\Request;

class SizeController extends Controller
{
    public function index()
    {
        $sizes=size::all();
    //    $deleteItem=Role::onlyTrashed()->count();
        return view('backend.size.index', compact('sizes'));
    }


}
