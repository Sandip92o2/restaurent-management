<?php

namespace App\Http\Controllers;

use Exception;
use App\Models\Order;
use Illuminate\Http\Request;
use App\Http\Requests\OrderRequest;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    public function index()
    {
        if(Auth::id())
        {
        $orders=Order::all();
        $deleteItem=Order::onlyTrashed()->count();
        return view('backend.orders.index', compact('orders', 'deleteItem'));
        }
        else{
            return redirect('login');
        }
    }

    
    public function show($id)
    {
        $order=Order::find($id);
        return view('backend.orders.show', compact('order'));
    }

    public function create()
    {
        return view('backend.orders.create');
    }

    public function insert(Request $request, $id )
    {   
        
       foreach($request->foodname as $key => $foodname)
       {    

            if(Auth::id()){
                $user_id=Auth::id();
                $item_id=$id;
                $data=new Order;
                $data->user_id=$user_id;
                $data->item_id=$item_id;
                $data->foodname=$foodname;
                $data->price=$request->price[$key];
                $data->quantity=$request->quantity[$key];
    
                $data->name=$request->name;
                $data->address=$request->address;
                $data->phone_num=$request->phone_num;
                $data->email=$request->email;
                $data->save();
                
                return redirect()->back()->withMessage('Success Order');
            }
     
       }
       

    }

    public function edit($id)
    {
        $order=Order::find($id);
        return view('backend.orders.edit', compact('order'));
    }

    public function Update(OrderRequest $request, $id)
    {   
        try{
            // data collect
            $data=$request->except('_token');
            
            // data insert
            Order::where('id', $id)->Update($data);
           
            // move to index page
            return redirect()->route('order_index')->withMessage('Updated Success');
        }catch(Exception $e){
            // validation eror
           return redirect()->route('order_insert')->withErrors($e->getMessage());
        }
    }

    public function delete($id)
    {
        $order=Order::find($id);
        $order->delete();
        return redirect()->back()->withMessage('Deleted Success');
    }

    public function trashlist()
    {
        $orders=Order::onlyTrashed()->get();
        return view('backend.orders.trashlist', compact('orders'));
    }

    public function restore($id)
    {
        Order::withTrashed()->where('id', $id)->restore();
        return redirect()->route('order_index')->withMessage('Restore Success');
    }
}
