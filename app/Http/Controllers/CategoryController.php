<?php

namespace App\Http\Controllers;

use Image;
use Exception;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Http\Requests\CategoryRequest;

class CategoryController extends Controller
{
    public function index()
    {
        $categories=Category::all();
        $deleteItem=Category::onlyTrashed()->count();
        return view('backend.categories.index', compact('categories', 'deleteItem'));
    }

    public function show($id)
    {
        $category=Category::find($id);
        return view('backend.categories.show', compact('category'));
    }

    public function create()
    {
        return view('backend.categories.create');
    }

    public function insert(CategoryRequest $request)
    {   
        try{
            
            // data collect
            $data=$request->all();

           if($request->image){
           $image = $this->uploadedImage($request->name, $request->image);
           }
           

           $data['image'] = $image;
           
            // data insert
            Category::create($data);
            // move to index page
            return redirect()->route('category_index')->withMessage('Inserted Success');
        }catch(Exception $e){
            // validation eror
           return redirect()->route('category_insert')->withErrors($e->getMessage());
        }
    }

    public function edit($id)
    {
        $category=Category::find($id);
        return view('backend.categories.edit', compact('category'));
    }

    public function Update(CategoryRequest $request, $id)
    {   
        try{
            // data collect
            $data=$request->except('_token');
              // image update
              if($request->hasFile('image')){
                $category = Category::where('id', $id)->first();

                $this->unlink($category->image);

                $data['image'] = $this->uploadedImage($request->name, $request->image);
            } 
            // data insert
            Category::where('id', $id)->Update($data);
            // move to index page
            return redirect()->route('category_index')->withMessage('Updated Success');
        }catch(Exception $e){
            // validation eror
           return redirect()->route('category_insert')->withErrors($e->getMessage());
        }
    }

    public function delete($id)
    {
        $category=Category::find($id);
        $category->delete();
        return redirect()->back()->withMessage('Deleted Success');
    }

    public function trashlist()
    {
        $categories=Category::onlyTrashed()->get();
        return view('backend.categories.trashlist', compact('categories'));
    }

    public function restore($id)
    {
        Category::withTrashed()->where('id', $id)->restore();
        return redirect()->route('category_index');

    }


    public function uploadedImage($name, $image)
    {
        //name ba title er nam uniq korar jonno  
        $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
        
        $file_name = $timestamp .'-'.$name. '.' . $image->getClientOriginalExtension();
        // storage e photo 
        $pathToUpload = storage_path().'/app/public/categories/';  // image  upload application save korbo
        // automatic folader create...
        if(!is_dir($pathToUpload)) {

            mkdir($pathToUpload, 0755, true);

        }

        Image::make($image)->resize(634,792)->save($pathToUpload.$file_name);

        return $file_name;
    }

    private function unlink($image)
    {
        $pathToUpload = storage_path().'/app/public/categories/';
        if ($image != '' && file_exists($pathToUpload. $image)) {
            @unlink($pathToUpload. $image);
        }

    }
}

