<?php

namespace App\Http\Controllers;

use Image;
use Exception;
use App\Models\chef;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class ChefController extends Controller
{
    public function index()
    {
        $chefs=chef::all();
       
        return view('backend.chefs.index', compact('chefs'));
    }

    public function create()
    {
        return view('backend.chefs.create');
    }

    public function insert(Request $request)
    {   
        try{
            
            // data collect
            $data=$request->all();
            
           if($request->image){
           $image = $this->uploadedImage($request->name, $request->image);
           }
           

           $data['image'] = $image;
           
            // data insert
            chef::create($data);
            // move to index page
            return redirect()->route('chefs_index')->withMessage('Inserted Success');
        }catch(Exception $e){
            // validation eror
          dd($e->getMessage());
        }
    }
    
    public function uploadedImage($name, $image)
    {
        //name ba title er nam uniq korar jonno  
        $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
        
        $file_name = $timestamp .'-'.$name. '.' . $image->getClientOriginalExtension();
        // storage e photo 
        $pathToUpload = storage_path().'/app/public/chefs/';  // image  upload application save korbo
        // automatic folader create...
        if(!is_dir($pathToUpload)) {

            mkdir($pathToUpload, 0755, true);

        }

        Image::make($image)->resize(634,792)->save($pathToUpload.$file_name);

        return $file_name;
    }

    private function unlink($image)
    {
        $pathToUpload = storage_path().'/app/public/chefs/';
        if ($image != '' && file_exists($pathToUpload. $image)) {
            @unlink($pathToUpload. $image);
        }

    }

    public function edit($id)
    {
        $chefs=chef::find($id);
        return view('backend.chefs.edit', compact('chefs'));
    }

    public function Update(Request $request, $id)
    {   
        try{
            // data collect
            $data=$request->except('_token');
              // image update
              if($request->hasFile('image')){
                $chefs = chef::where('id', $id)->first();

                $this->unlink($chefs->image);

                $data['image'] = $this->uploadedImage($request->name, $request->image);
            } 
            // data insert
            chef::where('id', $id)->Update($data);
            // move to index page
            return redirect()->route('chefs_index')->withMessage('Updated Success');
        }catch(Exception $e){
            // validation eror
           dd($e->getMessage());
        }
    }

    
    public function delete($id)
    {
        $chef=chef::find($id);
        $chef->delete();
        return redirect()->back()->withMessage('Deleted Success');
    }
}
