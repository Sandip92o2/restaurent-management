<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class userController extends Controller
{
    public function index()
    {
        $users = User::paginate(15);
        // $users=DB::table('users')->paginate(10);
        return view('backend.user.index', compact('users'));
    }

    
    public function delete($id)
    {
        $user=User::find($id);
        $user->delete();
        return redirect()->back()->withMessage('Deleted Success');
    }


}
