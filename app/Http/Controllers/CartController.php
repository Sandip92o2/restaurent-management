<?php

namespace App\Http\Controllers;

use App\Http\Requests\CartRequest;
use Exception;
use App\Models\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{
    public function addcart(Request $request, $id)
    {
        // cart here...
        if(Auth::id()){
            $user_id=Auth::id();
            $item_id=$id;
            $quantity=$request->quantity;
            $price=$request->price;
         
          

            $cart=new Cart;
            $cart->user_id=$user_id;
            $cart->item_id=$item_id;
            $cart->quantity=$quantity;
            $cart->price=$price;
        
            $cart->save();
            
            
            return redirect()->back();
        }
        else{
            return redirect('/login');
        }
    }

    public function index()
    {
        $carts=Cart::all();
       
        return view('backend.carts.index', compact('carts'));
    }

    public function show($id)
    {
        $card=Cart::find($id);
        return view('backend.carts.show', compact('card'));
    }

    public function create()
    {
        return view('backend.carts.create');
    }

    public function insert(CartRequest $request)
    {   
        try{
            // data collect
            $data=$request->all();
            // data insert
            Cart::create($data);
            // move to index page
            return redirect()->route('cart_index')->withMessage('Inserted Success');
        }catch(Exception $e){
            // validation eror
           return redirect()->route('cart_insert')->withErrors($e->getMessage());
        }
    }

    public function edit($id)
    {
        $cart=Cart::find($id);
        return view('backend.carts.edit', compact('cart'));
    }

    public function Update(CartRequest $request, $id)
    {   
        try{
            // data collect
            $data=$request->except('_token');
            
            // data insert
            Cart::where('id', $id)->Update($data);
           
            // move to index page
            return redirect()->route('cart_index')->withMessage('Updated Success');
        }catch(Exception $e){
            // validation eror
           return redirect()->route('cart_insert')->withErrors($e->getMessage());
        }
    }

    public function delete($id)
    {
        $cart=Cart::find($id);
        $cart->delete();
        return redirect()->back()->withMessage('Deleted Success');
    }

    public function trashlist()
    {
        $carts=Cart::onlyTrashed()->get();
        return view('backend.carts.trashlist', compact('carts'));
    }

    public function restore($id)
    {
        Cart::withTrashed()->where('id', $id)->restore();
        return redirect()->route('cart_index');
    }
}
