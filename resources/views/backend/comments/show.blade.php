@extends('backend.master')

@section('main_content')

<div class="card">
    <div class="card-body">
    <h5 class="card-title">Data Show:</h5>

    <p class="card-text">User Id: {{$comment->user_id ?? 'no user id'}}</p>
    <p class="card-text">Item Id: {{$comment->item_id ?? 'no item id'}}</p>
    <p class="card-text">Comment: {{$comment->comment ?? 'no comment'}}</p>
    </div>
  </div>

@endsection
