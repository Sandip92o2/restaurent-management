@extends('backend.master')

@section('main_content')

{{-- Start Success Message --}}
@if (Session::has('message'))
<P class="alert alert-success m-2">{{Session::get('message')}}</P>
@endif
{{-- end success Message --}}

{{-- table container --}}
<div class="container">
    <div class="card shadow">

        <div class="card-header bg-info text-light ">
          <h4> Comment List</h4>
        </div>
        
        <div class="card-body p-3">
            <a class="btn btn-sm btn-primary" href="{{route('comment_create')}}"><i class="bi bi-plus-lg"></i>Add new comment</a>
            <a class="btn btn-sm btn-danger" href="{{route('comment_trashlist')}}"><i class="bi bi-trash"></i><span>{{$deleteItem ?? 'o'}}</span>  Trash List </a>
            <a class="btn btn-sm btn-success" href=""><i class="bi bi-file-earmark-pdf"></i></i> Download Pdf</a>
            <a class="btn btn-sm btn-dark" href=""><i class="bi bi-file-earmark-excel"></i> Download Excel</a>

            {{-- strat table --}}
            <table class="table table-sm table-bordered mt-3">
                <thead>
                  <tr class="table-dark text-white">
                    <th scope="col">Seriol Num</th>
                    <th scope="col">User id</th>
                    <th scope="col">Item id</th>
                    <th scope="col">Comment</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody>
              
                {{-- start php --}}
                @php
                    $i=1
                @endphp
                {{-- end php --}}

                {{-- Start foreach  --}}
                @foreach ($comments as $comment)
                    <tr>
                        <th scope="row" class="text-center">{{$i++}}</th>
                        <td>{{$comment->user_id ?? 'no user id'}}</td>
                        <td>{{$comment->_id ?? 'no item id'}}</td>
                        <td>{{$comment->comment ?? 'no comment'}}</td>
                        <td>
                        <a href="{{route('comment_show', $comment->id)}}" class="btn btn-sm btn-primary"><i class="bi bi-eye"></i>Show</a>
                        <a href="{{route('comment_edit', $comment->id)}}" class="btn btn-sm btn-warning"><i class="bi bi-pencil"></i>Edit</a>
                        <a href="{{route('comment_delete', $comment->id)}}" class="btn btn-sm btn-danger"><i class="bi bi-trash3"></i>Delete</a>
                        </td>
                    </tr>
                @endforeach
                {{-- End foreach--}}
                </tbody>
              </table>
              {{-- end table --}}
        </div>
    </div>
 </div>

@endsection