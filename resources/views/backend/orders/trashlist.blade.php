@extends('backend.master')

@section('main_content')

{{-- Start Success Message --}}
@if (Session::has('message'))
<P class="alert alert-success m-2">{{Session::get('message')}}</P>
@endif
{{-- end success Message --}}

{{-- table container --}}
<div class="container">
    <div class="card shadow">

        <div class="card-header bg-info text-light ">
          <h4> Order List</h4>
        </div>
        
        <div class="card-body p-3">
            <a class="btn btn-sm btn-primary" href="{{route('order_index')}}"><i class="bi bi-skip-backward"></i> Back</a>
            
            {{-- strat table --}}
            <table class="table table-sm table-bordered mt-3">
                <thead>
                  <tr class="table-dark text-white">
                    <th scope="col">Seriol Num</th>
                    <th scope="col">User id</th>
                    <th scope="col">Item id</th>
                    <th scope="col">Qantity</th>
                    <th scope="col">Unit price</th>
                    <th scope="col">Total price</th>
                    <th scope="col">Address</th>
                    <th scope="col">Phone Number</th>
                    <th scope="col">Email</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody>
              
                {{-- start php --}}
                @php
                    $i=1
                @endphp
                {{-- end php --}}

                {{-- Start foreach  --}}
                @foreach ($orders as $order)
                    <tr>
                        <th scope="row" class="text-center">{{$i++}}</th>
                        <td>{{$order->user_id ?? 'no user id'}}</td>
                        <td>{{$order->item_id ?? 'no item id'}}</td>
                        <td>{{$order->quantity ?? 'no quantity'}}</td>
                        <td>{{$order->unit_price ?? 'no unit price'}}</td>
                        <td>{{$order->total_price ?? 'no total price'}}</td>
                        <td>{{$order->address ?? 'no address'}}</td>
                        <td>{{$order->Phone_num ?? 'no phone num'}}</td>
                        <td>{{$order->email ?? 'no email'}}</td>
                        <td>
                          
                            <a href="{{route('order_restore', $order->id)}}" class="btn btn-sm btn-primary"><i class="bi bi-recycle"></i> Restore</a>
                        </td>
                    </tr>
                @endforeach
                {{-- End foreach--}}
                </tbody>
              </table>
              {{-- end table --}}
        </div>
    </div>
 </div>

@endsection