@extends('backend.master')

@section('main_content')

{{-- Start Success Message --}}
@if (Session::has('message'))
<P class="alert alert-success m-2">{{Session::get('message')}}</P>
@endif
{{-- end success Message --}}

{{-- table container --}}

    <div class="card shadow">

        <div class="card-header bg-info text-light ">
          <h4> Order List</h4>
        </div>
        
        <div class="card-body p-3">
            <a class="btn btn-sm btn-primary" href="{{route('order_create')}}"><i class="bi bi-plus-lg"></i>Add new order</a>
            <a class="btn btn-sm btn-danger" href="{{route('order_trashlist')}}"><i class="bi bi-trash"></i><span>{{$deleteItem ?? '0'}}</span>  Trash List </a>
            <a class="btn btn-sm btn-success" href=""><i class="bi bi-file-earmark-pdf"></i></i> Download Pdf</a>
            <a class="btn btn-sm btn-dark" href=""><i class="bi bi-file-earmark-excel"></i> Download Excel</a>
          <br>
          {{-- search --}}
            <form class="search-form d-flex align-items-center" method="get" action="{{route('search')}}">
              @csrf
              <input type="text" name="search" placeholder="Search">
              <button class="btn btn-primary" type="submit"><i class="bi bi-search"></i></button>
            </form>
            {{-- strat table --}}
            
            <table class="table table-sm table-bordered mt-3">
           
                <thead>
                  <tr class="table-dark text-white">
                    <th scope="col">Serial Num</th>
                    <th scope="col">User id</th>
                    <th scope="col">Item id</th>
                    <th scope="col">Food name</th>
                    <th scope="col" >Qantity</th>
                    <th scope="col" >Name</th>
                    <th scope="col">price</th>
                    <th scope="col">Total price</th>
                    <th scope="col">Address</th>
                    <th scope="col">Phone Number</th>
                    <th scope="col">Email</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody>
              
                {{-- start php --}}
                @php
                    $i=1
                @endphp
                {{-- end php --}}

                {{-- Start foreach  --}}
                @foreach ($orders as $order)
                    <tr>
                        <th scope="row" class="text-center">{{$i++}}</th>
                        <td>{{$order->user_id ?? 'no user id'}}</td>
                        <td>{{$order->item_id ?? 'no item id'}}</td>
                        <td>{{$order->foodname ?? 'no foodname'}}</td>
                        <td>{{$order->quantity ?? 'no quantity'}}</td>
                        <td>{{$order->name ?? 'no name'}}</td>
                        <td>{{$order->price ?? 'no unit price'}} BDT</td>
                        <td>{{$order->price * $order->quantity?? 'no total price'}} BDT</td>
                        <td>{{$order->address ?? 'no address'}}</td>
                        <td>{{$order->phone_num ?? 'no phone num'}}</td>
                        <td>{{$order->email ?? 'no email'}}</td>
                        <td>
                          <div class="d-flex">
                            <a href="{{route('order_show', $order->id)}}" class="btn btn-sm btn-primary"><i class="bi bi-eye"></i>Show</a>
                            <a href="{{route('order_edit', $order->id)}}" class="btn btn-sm btn-warning"><i class="bi bi-pencil"></i>Edit</a>
                            <a href="{{route('order_delete', $order->id)}}" class="btn btn-sm btn-danger"><i class="bi bi-trash3"></i>Delete</a>
                     
                          </div>
                           
                       
                        </td>
                    </tr>
                @endforeach
                {{-- End foreach--}}
                </tbody>
               
              </table>
              
              {{-- end table --}}
        </div>
    </div>


@endsection