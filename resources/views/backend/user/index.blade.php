@extends('backend.master')

@section('main_content')

{{-- Start Success Message --}}
@if (Session::has('message'))
<P class="alert alert-success m-2">{{Session::get('message')}}</P>
@endif
{{-- end success Message --}}

{{-- table container --}}
<div class="container">
    <div class="card shadow">

        <div class="card-header bg-info text-light ">
          <h4>Users List</h4>
        </div>
        
        <div class="card-body p-3">
  
            <a class="btn btn-sm btn-success" href=""><i class="bi bi-file-earmark-pdf"></i></i> Download Pdf</a>
            <a class="btn btn-sm btn-dark" href=""><i class="bi bi-file-earmark-excel"></i> Download Excel</a>

            {{-- strat table --}}
            <table class="table table-sm table-busered mt-3">
                <thead>
                  <tr class="table-dark text-white">
                    <th scope="col">Seriol Num</th>
                    
                    <th scope="col">Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">User Type id</th>
                    <th scope="col">User Type Action</th>
                    
                  </tr>
                </thead>
                <tbody>
              
                {{-- start php --}}
                @php
                    $i=1
                @endphp
                {{-- end php --}}

                {{-- Start foreach  --}}
                @foreach ($users as $user)
                    <tr>
                        <th scope="row" class="text-center">{{$i++}}</th>
                        <td>{{$user->name ?? 'no name'}}</td>
                        <td>{{$user->email ?? 'no name'}}</td>
                        <td><span class="badge bg-success">{{user_type_name($user->user_type_id) ?? 'no name'}}</span></td>
                        {{-- usertype here --}}
                        @if ($user->user_type_id=='4')
                        <td>
                            <div class="div d-flex">
                              <a href="{{route('user_delete', $user->id)}}" class="btn btn-sm btn-danger"><i class="bi bi-trash3"></i>Delete</a>
                              <a href="" class="btn btn-sm btn-primary"><i class="bi bi-trash3"></i>Edit</a>
                            </div>
                          </td>
                        @else
                        <td>Not Allowed</td>
                        @endif
                      
                    </tr>
                @endforeach
                {{-- End foreach--}}
                </tbody>
              </table>

              {{$users->links('pagination::bootstrap-5')}}
              {{-- end table --}}
        </div>
    </div>
 </div>

@endsection