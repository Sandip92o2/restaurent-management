@extends('backend.master')

@section('main_content')

<div class="container">

    <div class="card shadow">
        <div class="card-header bg-primary-subtle text-emphasis-primary"><h4>Edit Item</h4></div>

        <div class="card-body p-2">
            {{-- start form --}}
            <form action="{{route('item_update', $item->id)}}" method="POST" enctype="multipart/form-data">
                {{-- token --}}
                @csrf
                {{-- end token --}}
                    <div class="div">
                        <label>Name:</label>
                        <input type="text" name="name" value="{{$item->name ?? 'no name'}}" placeholder="create name" class="form-control">
                    </div>
                   <br>

                   <div class="div">
                    <label>Old Image:</label> <br>
                    <img height="150" width="150" src="{{ asset('storage/items/'. $item->image) }}">
                    </div>

                   <div class="div">
                    <label for="image">New Image</label>
                    <input type="file" class="form-control" name="image">
                  </div>


                  <div class="div">
                    <label>Price:</label>
                    <input type="number" name="price" value="{{$item->price ?? 'no price'}}" class="form-control">
                </div>

                <div class="div">
                    <label>Discount:</label>
                    <input type="number" name="discount" value="{{$item->discount ?? 'no discount'}}" class="form-control">
                </div>
                <div>
                    <div><button type="submit" class="btn btn-sm btn-primary m-3">Update</button></div>
                </div>
            </form>
            {{-- end form --}}
        </div>
    </div>
</div>



@endsection
