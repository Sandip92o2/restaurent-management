@extends('backend.master')

@section('main_content')

<div class="card">
  <div class="card-body">
    <h5 class="card-title">Item Show:</h5>
    <p class="card-text"><i><b>Category Id:</b></i> {{$item->category_id ?? 'no id'}}</p>
    <p class="card-text"><i><b>Name:</b></i> {{$item->name ?? 'no name'}}</p>
    <p class="card-text"><i><b>Image:</b></i> {{$item->image ?? 'no image'}}</p>
    <p class="card-text"><i><b>Price:</b></i> {{$item->price ?? 'no price'}}</p>
    <p class="card-text"><i><b>Discount:</b></i> {{$item->discount ?? 'no discount'}}</p>
  </div>
</div>

@endsection
