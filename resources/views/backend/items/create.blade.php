@extends('backend.master')

@section('main_content')

<div class="container">

    <div class="card shadow">
        <div class="card-header bg-primary-subtle text-emphasis-primary"><h4>Create Item</h4></div>

        <div class="card-body p-2">
            {{-- start form --}}
            <form action="{{route('item_insert')}}" method="POST" enctype="multipart/form-data">
                {{-- token --}}
                @csrf
                {{-- end token --}}

                {{-- Category data  --}}
                    <label>Seclect Category:</label>
                    <select name="category_id" id="" class="form-control">
                        
                            @foreach ($categories as $category)
                                <option value="{{$category->id}}">
                                    {{$category->name}}
                                </option>
                            @endforeach
                       
                    </select>

                {{-- category data end --}}
                    <br> 
                    <div class="div">
                        <label>Name:</label>
                        <input type="text" name="name" placeholder="item name" class="form-control">
                    </div>
                  
                    
                   <br>
               
                   <div class="div">
                    <label for="image">Image Uploaded</label>
                    <input type="file" class="form-control" name="image">
                  </div>

                <div class="div">
                    <label>Price:</label>
                    <input type="number" name="price" class="form-control">
                </div>

                <div class="div">
                    <label>Discount:</label>
                    <input type="number" name="discount" class="form-control">
                </div>

                <div>
                    <div><button type="submit" class="btn btn-sm btn-primary m-3">Save</button></div>
                </div>
            </form>
            {{-- end form --}}
        </div>
    </div>
</div>



@endsection
