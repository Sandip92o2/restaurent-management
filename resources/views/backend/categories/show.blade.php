@extends('backend.master')

@section('main_content')

<div class="card">
    <div class="card-body">
    <h5 class="card-title">Data Show:</h5>

    <p class="card-text">Category Name: {{$category->name ?? 'no title'}}</p>
    <p class="card-text">Category Description: {{$category->description ?? 'no title'}}</p>
    <p class="card-text">Category Image: {{$category->image ?? 'no image'}}</p>
    </div>
  </div>

@endsection
