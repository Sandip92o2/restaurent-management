@extends('backend.master')

@section('main_content')

<div class="container">

    <div class="card shadow">
        <div class="card-header bg-primary-subtle text-emphasis-primary"><h4>Create Category</h4></div>

        <div class="card-body p-2">
            {{-- start form --}}
            <form action="{{route('category_insert')}}" method="POST" enctype="multipart/form-data">
                {{-- token --}}
                @csrf
                {{-- end token --}}
                    <div class="div">
                        <label>Name:</label>
                        <input type="text" name="name" value="{{old('name')}}" placeholder="create name" class="form-control">
                    </div>
                    {{-- validation error --}}
                    @error('name')
                        <span class="text-danger">{{ $message }}</span>
                    @enderror
                    {{-- end validation error --}}
                    <div class="div">
                        <label>Description:</label>
                        <textarea type="text" id="editor"  name="description" class="form-control">  {{old('description')}} </textarea>
                    </div>
                 
                   <div class="div">
                    <label for="image">Image Uploaded</label>
                    <input type="file" class="form-control" name="image">
                  </div>
                <div>
                    <div><button type="submit" class="btn btn-sm btn-primary m-3">Save</button></div>
                </div>
            </form>
            {{-- end form --}}
        </div>
    </div>
</div>



@endsection
