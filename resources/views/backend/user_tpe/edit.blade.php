@extends('backend.master')

@section('main_content')

<div class="container">

    <div class="card shadow">
        <div class="card-header bg-primary-subtle text-emphasis-primary"><h4>Role Create</h4></div>

        <div class="card-body p-2">
            {{-- start form --}}
            <form action="{{route('role_update', $role->id)}}" method="POST">
                {{-- token --}}
                @csrf
                {{-- end token --}}
                    <div class="div">
                        <label>Name:</label>
                        <input type="text" name="name"  value="{{$role->name}}"  placeholder="name" class="form-control">
                    </div>
                <div>
                    <div><button type="submit" class="btn btn-sm btn-primary m-3">Update</button></div>
                </div>
            </form>
            {{-- end form --}}
        </div>
    </div>
</div>



@endsection
