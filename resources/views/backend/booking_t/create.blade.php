@extends('backend.master')

@section('main_content')

<div class="container">

    <div class="card shadow">
        <div class="card-header bg-primary-subtle text-emphasis-primary"><h4>Table Booking Create</h4></div>

        <div class="card-body p-2">
            {{-- start form --}}
            <form action="{{route('t_booking_insert')}}" method="POST">
                {{-- token --}}
                @csrf
                {{-- end token --}}
                    <div class="div">
                        <label>Name:</label>
                        <input type="text" name="name"  value="{{old('name')}}"  placeholder="name" class="form-control">
                    </div>

                {{-- validation error --}}
                @error('name')
                <span class="text-danger">{{ $message }}</span>
                @enderror
                {{-- end validation error --}}

                    <div class="div">
                        <label>Email:</label>
                        <input type="email" name="email"  value="{{old('email')}}"  placeholder="email" class="form-control">
                    </div>
                
                {{-- validation error --}}
                @error('email')
                <span class="text-danger">{{ $message }}</span>
                @enderror
                {{-- end validation error --}}
                    <div class="div">
                        <label>Date of Time:</label>
                        <input type="date" name="d_o_t"  value="{{old('d_o_t')}}"  placeholder="date of time" class="form-control">
                    </div>
                {{-- validation error --}}
                @error('d_o_t')
                <span class="text-danger">{{ $message }}</span>
                @enderror
                {{-- end validation error --}}
                    <div class="div">
                        <label>Number of people:</label>
                        <input type="number" name="n_o_p"  value="{{old('n_o_p)')}}"  placeholder="num of people" class="form-control">
                    </div>
                {{-- validation error --}}
                @error('n_o_b')
                <span class="text-danger">{{ $message }}</span>
                @enderror
                {{-- end validation error --}}     

                    <div class="div">
                        <label>Special Request:</label>
                        <textarea type="text" name="special_rq" class="form-control">
                            {{old('special-rq')}}
                        </textarea>
                    </div>
            {{-- validation error --}}
            @error('special_rq')
            <span class="text-danger">{{ $message }}</span>
            @enderror
            {{-- end validation error --}}

                    <div>
                        <div><button type="submit" class="btn btn-sm btn-primary m-3">Save</button></div>
                    </div>
            </form>
            {{-- end form --}}
        </div>
    </div>
</div>
{{-- <div class="div">
    <label>Specical request:</label>
    <textarea type="text" name="s_rq"  value=""  placeholder="special request" class="form-control">
</div> --}}



@endsection
