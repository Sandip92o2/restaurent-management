@extends('backend.master')

@section('main_content')

{{-- Start Success Message --}}
@if (Session::has('message'))
<P class="alert alert-success m-2">{{Session::get('message')}}</P>
@endif
{{-- end success Message --}}

{{-- table container --}}
<div class="container">
    <div class="card shadow">

        <div class="card-header bg-info text-light ">
          <h4> Table Booking List</h4>
        </div>
        
        <div class="card-body p-3">
            <a class="btn btn-sm btn-primary" href="{{route('t_booking_create')}}"><i class="bi bi-plus-lg"></i>Add new Booking</a>
            <a class="btn btn-sm btn-danger" href="{{route('t_booking_trashlist')}}"><i class="bi bi-trash"></i><span>{{$deleteItem ?? '0'}}</span>  Trash List </a>
            <a class="btn btn-sm btn-success" href=""><i class="bi bi-file-earmark-pdf"></i></i> Download Pdf</a>
            <a class="btn btn-sm btn-dark" href=""><i class="bi bi-file-earmark-excel"></i> Download Excel</a>

            {{-- strat table --}}
            <table class="table table-sm table-bordered mt-3">
                <thead>
                  <tr class="table-dark text-white">
                    <th scope="col">Serial num</th>
                    <th scope="col">User id</th>
                    <th scope="col">Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Phone Num</th>
                    <th scope="col">Date</th>
                    <th scope="col">Time</th>
                    <th scope="col">Num of People</th>
                    <th scope="col">Message</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody>
              
                {{-- start php --}}
                @php
                    $i=1
                @endphp
                {{-- end php --}}

                {{-- Start foreach  --}}
                @foreach ($tables as $table)
                    <tr>
                        <th scope="row" class="text-center">{{$i++}}</th>
                        <td>{{$table->user_id ?? 'no user id'}}</td>
                        <td>{{$table->name ?? 'no name'}}</td>
                        <td>{{$table->email ?? 'no email'}}</td>
                        <td>{{$table->p_num ?? 'no number'}}</td>
                        <td>{{$table->date?? 'no date'}}</td>
                        <td>{{$table->time?? 'no time'}}</td>
                        <td>{{$table->n_o_p ?? 'no number of people'}}</td>
                        <td>{{$table->message ?? 'no message '}}</td>
                        <td>
                        <div class="div d-flex">
                          <a href="{{route('t_booking_delete', $table->id)}}" class="btn btn-sm btn-danger"><i class="bi bi-trash3"></i>Delete</a>
                        </td>
                        </div>
                    </tr>
                @endforeach
                {{-- End foreach--}}
                </tbody>
              </table>
              {{-- end table --}}
        </div>
    </div>
 </div>

@endsection