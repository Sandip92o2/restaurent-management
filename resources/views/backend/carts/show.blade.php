@extends('backend.master')

@section('main_content')

<div class="card">
    <div class="card-body">
    <h5 class="card-title">Data Show:</h5>

    <p class="card-text">Cart User Id: {{$cart->user_id ?? 'no user id'}}</p>
    <p class="card-text">Cart Item Id: {{$cart->item_id ?? 'no item id'}}</p>
    <p class="card-text">Carts Quantity: {{$cart->quantity ?? 'no quantity'}}</p>
    <p class="card-text">Carts Unit Price: {{$cart->unit_price ?? 'no unit price'}}</p>
    <p class="card-text">Carts Total Price: {{$cart->total_price ?? 'no total price'}}</p>
    </div>
  </div>

@endsection
