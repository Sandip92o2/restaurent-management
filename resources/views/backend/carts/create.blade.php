@extends('backend.master')

@section('main_content')

<div class="container">

    <div class="card shadow">
        <div class="card-header bg-primary-subtle text-emphasis-primary"><h4>Create Cart</h4></div>

        <div class="card-body p-2">
            {{-- start form --}}
            <form action="{{route('cart_insert')}}" method="POST">
                {{-- token --}}
                @csrf
                {{-- end token --}}
                    <div class="div">
                        <label>Quantity:</label>
                        <input type="number" name="quantity"  value="{{old('quantity')}}"  placeholder="" class="form-control">
                    </div>
            
                    <div class="div">
                        <label>Unit_price:</label>
                        <input type="number"  name="unit_price" value="{{old('unit_price')}}" class="form-control">
                    </div>
                    {{-- validation error --}}
                    @error('unit_price')
                        <span class="text-danger">{{ $message }}</span>
                    @enderror
                    {{-- end validation error --}}
                    <br>
                    <div class="div">
                        <label>Total_price:</label>
                        <input type="number"  name="total_price" value="{{old('total_price')}}" class="form-control">
                    </div>
                    {{-- validation error --}}
                    @error('total_price')
                        <span class="text-danger">{{ $message }}</span>
                     @enderror
                     {{-- end validation error --}}
                   
                    </div>
                <div>
                    <div><button type="submit" class="btn btn-sm btn-primary m-3">Save</button></div>
                </div>
            </form>
            {{-- end form --}}
        </div>
    </div>
</div>



@endsection
