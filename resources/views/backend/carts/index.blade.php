@extends('backend.master')

@section('main_content')

{{-- Start Success Message --}}
@if (Session::has('message'))
<P class="alert alert-success m-2">{{Session::get('message')}}</P>
@endif
{{-- end success Message --}}

{{-- table container --}}
<div class="container">
    <div class="card shadow">

        <div class="card-header bg-info text-light ">
          <h4> Card List</h4>
        </div>
        
        <div class="card-body p-3">
            <a class="btn btn-sm btn-primary" href="{{route('cart_create')}}"><i class="bi bi-plus-lg"></i>Add new cart</a>
            <a class="btn btn-sm btn-danger" href="{{route('cart_trashlist')}}"><i class="bi bi-trash"></i><span>{{$deleteItem ?? '0'}}</span>  Trash List </a>
            <a class="btn btn-sm btn-success" href=""><i class="bi bi-file-earmark-pdf"></i></i> Download Pdf</a>
            <a class="btn btn-sm btn-dark" href=""><i class="bi bi-file-earmark-excel"></i> Download Excel</a>

            {{-- strat table --}}
            <table class="table table-sm table-bordered mt-3">
                <thead>
                  <tr class="table-dark text-white">
                    <th scope="col">Seriol Num</th>
                    <th scope="col">User id</th>
                    <th scope="col">Item id</th>     
                    <th scope="col">qantity</th>
                    <th scope="col">price</th>
                    <th scope="col">total price</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody>
              
                {{-- start php --}}
                @php
                    $i=1
                @endphp
                {{-- end php --}}

                {{-- Start foreach  --}}
                @foreach ($carts as $cart)
                    <tr>
                        <th scope="row" class="text-center">{{$i++}}</th>
                        <td>{{$cart->user_id ?? 'no user id'}}</td>
                        <td>{{$cart->item_id ?? 'no item id'}}</td>
              
                        <td>{{$cart->quantity ?? 'no quantity'}}</td>
                        <td>{{$cart->price ?? 'no unit price'}}</td>
                        <td>{{$cart->price * $cart->quantity ?? 'no total price'}}</td>
                        <td>
                        <a href="{{route('cart_show', $cart->id)}}" class="btn btn-sm btn-primary"><i class="bi bi-eye"></i>Show</a>
                        <a href="{{route('cart_edit', $cart->id)}}" class="btn btn-sm btn-warning"><i class="bi bi-pencil"></i>Edit</a>
                        <a href="{{route('cart_delete', $cart->id)}}" class="btn btn-sm btn-danger"><i class="bi bi-trash3"></i>Delete</a>
                        </td>
                    </tr>
                @endforeach
                {{-- End foreach--}}
                </tbody>
              </table>
              {{-- end table --}}
        </div>
    </div>
 </div>

@endsection