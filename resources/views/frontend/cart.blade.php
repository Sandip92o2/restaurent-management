<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Resturent</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">

    <!-- Favicon -->
    <link href="{{ asset('ui/frontend') }}/img/favicon.ico" rel="icon">

    <!-- Google Web Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Heebo:wght@400;500;600&family=Nunito:wght@600;700;800&family=Pacifico&display=swap" rel="stylesheet">

    <!-- Icon Font Stylesheet -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css" rel="stylesheet">

    <!-- Libraries Stylesheet -->
    <link href="{{ asset('ui/frontend') }}/lib/animate/animate.min.css" rel="stylesheet">
    <link href="{{ asset('ui/frontend') }}/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="{{ asset('ui/frontend') }}/lib/tempusdominus/css/tempusdominus-bootstrap-4.min.css" rel="stylesheet" />

    <!-- Customized Bootstrap Stylesheet -->
    <link href="{{ asset('ui/frontend') }}/css/bootstrap.min.css" rel="stylesheet">

    <!-- Template Stylesheet -->
    <link href="{{ asset('ui/frontend') }}/css/style.css" rel="stylesheet">
     {{-- je query --}}
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
</head>

<body>
    <div class="container-xxl bg-white p-0">
        <!-- Spinner Start -->
        <div id="spinner" class="show bg-white position-fixed translate-middle w-100 vh-100 top-50 start-50 d-flex align-items-center justify-content-center">
            <div class="spinner-border text-primary" style="width: 3rem; height: 3rem;" role="status">
                <span class="sr-only">Loading...</span>
            </div>
        </div>
        <!-- Spinner End -->


        {{-- nav --}}
        @include('frontend.includes.navbar')

        <div class="container-xxl py-5 bg-dark hero-header mb-5">
        </div>

        {{-- Start Success Message --}}
@if (Session::has('message'))
<P class="alert alert-success m-2">{{Session::get('message')}}</P>
@endif
{{-- end success Message --}}



        <div class="container">

            <table class="table table-sm table-bordered mt-3">
                <thead>
                  <tr class="table-dark text-white">
                    <th scope="col">Food Name</th>
                    <th scope="col">Qantity</th>
                    <th scope="col">Price</th>
                    <th scope="col">Total Price</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody>

               
              @foreach ($items as $item)
                                  {{-- form heere --}}
            <form action="{{route('order_insert', $item->id)}}" method="POST">
                @csrf
                <tr>
                    <td>
                        <input type="text" name="foodname[]" value="{{$item->name}} " hidden>
                        {{$item->name}}
                    </td>
                    <td>
                        <input type="text" name="quantity[]" value="{{$item->quantity}} " hidden>
                        {{$item->quantity}}
                    </td>
                    <td>
                        <input type="text" name="price[]" value="{{$item->price}} " hidden>
                        {{$item->price}}
                    </td>
                    <td>
                        <input type="text" name="total_price[]" value="{{$item->total_price}} " hidden>
                        {{$item->price * $item->quantity?? 'no total price'}}
                    </td>
                </tr>
         
              @endforeach
                {{-- delete --}}

            
              @foreach ($data as $data)
        
                <td class="table table-sm" style="position: relative; width:10ch; top: -100px; right: -900px;">
                    <a href="{{route('cart_item_delete', $data->id)}}" class="btn btn-danger">Delete</a>
                </td>
           

              @endforeach
              
                  
                </tbody>
              
                <tfoot>
                    <div>
                        <button href="" class="btn btn-success" type="button" id="order" >Order Now</button>
                    </div>
                    
                </tfoot>
            </table>
            
                
                <div class="div" id="appear" style="display: none">
                    <br>
                    <br>
                    <div class="card shadow" >
                        <div class="card-header bg-primary-subtle text-emphasis-primary"><h4>Order form</h4></div>
                
                        <div class="card-body p-2">

                                    <div class="div">
                                        <label>Name:</label>
                                        <input type="text"  name="name" value="{{old('name')}}" class="form-control">
                                    </div>
                                    {{-- validation error --}}
                                    @error('name')
                                        <span class="text-danger">{{ $message }}</span>
                                     @enderror
                                     {{-- end validation error --}}
                                
                                     <div class="div">
                                        <label>Address:</label>
                                        <input type="text"  name="address" value="{{old('address')}}" class="form-control">
                                    </div>
                
                                    {{-- validation error --}}
                                    @error('address')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                    {{-- end validation error --}}
                
                                    <div class="div">
                                        <label>Phone Num:</label>
                                        <input type="number"  name="phone_num" value="{{old('phone_num')}}" class="form-control">
                                    </div>
                
                                    {{-- validation error --}}
                                    @error('Phone_num')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                    {{-- end validation error --}}
                
                                    <div class="div">
                                        <label>Email Address:</label>
                                        <input type="email"  name="email" value="{{old('email')}}" class="form-control">
                                    </div>
                
                                    {{-- validation error --}}
                                    @error('email')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                    {{-- end validation error --}}
                                <div>
                                    <button class="btn btn-success mt-3" type="submit">Order Confrim</button>
                                    <button class="btn btn-danger mt-3" type="button" id="close">Order Cancel</button>
                                </div>
                    
                        </div>
            </form>

                    </div>
                </div>
                
   
        </div>

       







{{-- footer here --}}
    @include('frontend.includes.footer')


{{-- scripts --}}
<script type="text/javascript">
    $("#order").click(
        function()
        {
            $("#appear").show();
        }
    );

    $("#close").click(
        function()
        {
            $("#appear").hide();
        }
    );
</script>
<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"></script>
<script src="{{ asset('ui/frontend') }}/lib/wow/wow.min.js"></script>
<script src="{{ asset('ui/frontend') }}/lib/easing/easing.min.js"></script>
<script src="{{ asset('ui/frontend') }}/lib/waypoints/waypoints.min.js"></script>
<script src="{{ asset('ui/frontend') }}/lib/counterup/counterup.min.js"></script>
<script src="{{ asset('ui/frontend') }}/lib/owlcarousel/owl.carousel.min.js"></script>
<script src="{{ asset('ui/frontend') }}/lib/tempusdominus/js/moment.min.js"></script>
<script src="{{ asset('ui/frontend') }}/lib/tempusdominus/js/moment-timezone.min.js"></script>
<script src="{{ asset('ui/frontend') }}/lib/tempusdominus/js/tempusdominus-bootstrap-4.min.js"></script>

<!-- Template Javascript -->
<script src="{{ asset('ui/frontend') }}/js/main.js"></script>