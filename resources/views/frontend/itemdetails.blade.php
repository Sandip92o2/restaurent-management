<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Resturent</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">

    <!-- Favicon -->
    <link href="{{ asset('ui/frontend') }}/img/favicon.ico" rel="icon">

    <!-- Google Web Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Heebo:wght@400;500;600&family=Nunito:wght@600;700;800&family=Pacifico&display=swap" rel="stylesheet">

    <!-- Icon Font Stylesheet -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css" rel="stylesheet">

    <!-- Libraries Stylesheet -->
    <link href="{{ asset('ui/frontend') }}/lib/animate/animate.min.css" rel="stylesheet">
    <link href="{{ asset('ui/frontend') }}/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="{{ asset('ui/frontend') }}/lib/tempusdominus/css/tempusdominus-bootstrap-4.min.css" rel="stylesheet" />

    <!-- Customized Bootstrap Stylesheet -->
    <link href="{{ asset('ui/frontend') }}/css/bootstrap.min.css" rel="stylesheet">

    <!-- Template Stylesheet -->
    <link href="{{ asset('ui/frontend') }}/css/style.css" rel="stylesheet">
     {{-- je query --}}
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
</head>

<body>
    <div class="container-xxl bg-white p-0">
        <!-- Spinner Start -->
        <div id="spinner" class="show bg-white position-fixed translate-middle w-100 vh-100 top-50 start-50 d-flex align-items-center justify-content-center">
            <div class="spinner-border text-primary" style="width: 3rem; height: 3rem;" role="status">
                <span class="sr-only">Loading...</span>
            </div>
        </div>
        <!-- Spinner End -->


        {{-- nav --}}
        @include('frontend.includes.navbar')
        
               
                <div class="container-xxl py-5 bg-dark hero-header mb-5">
                </div>

                {{-- main --}}
                <div class="modal fade" id="productView" tabindex="-1">
                    <div class="modal-dialog modal-lg modal-dialog-centered">
                      <div class="modal-content overflow-hidden border-0">
                        <button class="btn-close p-4 position-absolute top-0 end-0 z-index-20 shadow-0" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
                        <div class="modal-body p-0">
                          <div class="row align-items-stretch">
                            <div class="col-lg-6 p-lg-0"><a class="glightbox product-view d-block h-100 bg-cover bg-center" style="background: url(img/product-5.jpg)" href="img/product-5.jpg" data-gallery="gallery1" data-glightbox="Red digital smartwatch"></a></div>
                            <div class="col-lg-6">
                              <div class="p-4 my-md-4">
                                <ul class="list-inline mb-2">
                                  <li class="list-inline-item m-0"><i class="fas fa-star small text-warning"></i></li>
                                 
                                </ul>
                                <h2 class="h4">Red digital smartwatch</h2>
                                <p class="text-muted">$250</p>
                                <p class="text-sm mb-4">Lorem ipsum dolor sit amet, consectetur adipisciing elt. In ut ullamcorper leo, eget euismod orci. Cum sociis natoque penatibus et magnis dis parturient montes nascetur ridiculus mus. Vestibulum ultricies aliquam convallis.</p>
                                <div class="row align-items-stretch mb-4 gx-0">
                                  <div class="col-sm-7">
                                    <div class="border d-flex align-items-center justify-content-between py-1 px-3"><span class="small text-uppercase text-gray mr-4 no-select">Quantity</span>
                                      <div class="quantity">
                                        <button class="dec-btn p-0"><i class="fas fa-caret-left"></i></button>
                                        <input class="form-control border-0 shadow-0 p-0" type="text" value="1">
                                        <button class="inc-btn p-0"><i class="fas fa-caret-right"></i></button>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-sm-5"><a class="btn btn-dark btn-sm w-100 h-100 d-flex align-items-center justify-content-center px-0" href="cart.html">Add to cart</a></div>
                                </div><a class="btn btn-link text-dark text-decoration-none p-0" href="#!"><i class="far fa-heart me-2"></i>Add to wish list</a>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <section class="py-5">
                    <div class="container">
                      <div class="row mb-5">
                        <div class="col-lg-6">
                          <!-- PRODUCT SLIDER-->
                          <div class="row m-sm-0">
                            <div class="col-sm-2 p-sm-0 order-2 order-sm-1 mt-2 mt-sm-0 px-xl-2">
                              <div class="swiper product-slider-thumbs">
                                <div class="swiper-wrapper ">
                                    @foreach ($items as $item)
                                    
                                        <div class=" h-100 w-100">
                                            <img class=" h-100 w-100" src="{{ asset('storage/items/'. $item->image ?? 'null') }}" alt="...">
                                        </div>
                            
                                    @endforeach
                                
                                 
                                </div>
                              </div>
                            </div>
                         
                          </div>
                        </div>
                        <!-- PRODUCT DETAILS-->
                        <div class="col-lg-6">
                          <ul class="list-inline mb-2 text-sm">
                            
                          </ul>
                          @foreach ($items as $item)
                          <form action="{{route('addcart', $item->id)}}" method="POST">
                            @csrf
                          <h1>{{$item->name ?? 'no name'}}</h1>
                          
                          <p class="text-sm mb-4"><del>{{ $item->price + 30 }}</del> {{ $item->price  }} BDT</p>
                          <p class="text-muted lead">{{$item->description ?? 'no description here'}} BDT</p>
                          <div class="row align-items-stretch mb-4">
                            
                              <div >Quantity</span>
                                <input class="d-flex" type="number" name="quantity" min="1" value="1" style="width: 100px; ">
                                <input class="btn btn-dark btn-sm d-flex justify-content" type="submit"  value="add to cart">
                              </div>
                            
                             
                            </form>
                            <br>
                          
                        </div>
                      </div>
                      <!-- DETAILS TABS-->
                      
                      {{-- description --}}
                      <ul class="nav nav-tabs border-0" id="myTab" role="tablist">
                        <li class="nav-item"><a class="nav-link text-uppercase active" id="description-tab" data-bs-toggle="tab" href="#description" role="tab" aria-controls="description" aria-selected="true">Description</a></li>
                        <li class="nav-item"><a class="nav-link text-uppercase" id="reviews-tab" data-bs-toggle="tab" href="#reviews" role="tab" aria-controls="reviews" aria-selected="false">Reviews</a></li>
                      </ul>
                      <div class="tab-content mb-5" id="myTabContent">
                        <div class="tab-pane fade show active" id="description" role="tabpanel" aria-labelledby="description-tab">
                          <div class="p-4 p-lg-5 bg-white">
                            <h6 class="text-uppercase">Product description </h6>
                            <p class="text-muted text-sm mb-0">{{$product->description ?? 'No Description here'}}</p>
                          </div>
                        </div>
                        @endforeach

                        {{-- review --}}
                        <div class="tab-pane fade" id="reviews" role="tabpanel" aria-labelledby="reviews-tab">
                          <div class="p-4 p-lg-5 bg-white">
                            <div class="row">
                              <div class="col-lg-8">
                                <div class="d-flex mb-3">
                                  <div class="flex-shrink-0"><img class="rounded-circle" src="img/customer-1.png" alt="" width="50"/></div>
                                  <div class="ms-3 flex-shrink-1">
                                    <h6 class="mb-0 text-uppercase">Jason Doe</h6>
                                    <p class="small text-muted mb-0 text-uppercase">20 May 2020</p>
                                    <ul class="list-inline mb-1 text-xs">
                                      <li class="list-inline-item m-0"><i class="fas fa-star text-warning"></i></li>
                                      <li class="list-inline-item m-0"><i class="fas fa-star text-warning"></i></li>
                                      <li class="list-inline-item m-0"><i class="fas fa-star text-warning"></i></li>
                                      <li class="list-inline-item m-0"><i class="fas fa-star text-warning"></i></li>
                                      <li class="list-inline-item m-0"><i class="fas fa-star-half-alt text-warning"></i></li>
                                    </ul>
                                    <p class="text-sm mb-0 text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                  </div>
                                </div>
                             
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                   
                    </div>
                  </section>
             





                </div>




                {{-- footer here --}}
    @include('frontend.includes.footer')

    {{-- screpts --}}
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"></script>
    <script src="{{ asset('ui/frontend') }}/lib/wow/wow.min.js"></script>
    <script src="{{ asset('ui/frontend') }}/lib/easing/easing.min.js"></script>
    <script src="{{ asset('ui/frontend') }}/lib/waypoints/waypoints.min.js"></script>
    <script src="{{ asset('ui/frontend') }}/lib/counterup/counterup.min.js"></script>
    <script src="{{ asset('ui/frontend') }}/lib/owlcarousel/owl.carousel.min.js"></script>
    <script src="{{ asset('ui/frontend') }}/lib/tempusdominus/js/moment.min.js"></script>
    <script src="{{ asset('ui/frontend') }}/lib/tempusdominus/js/moment-timezone.min.js"></script>
    <script src="{{ asset('ui/frontend') }}/lib/tempusdominus/js/tempusdominus-bootstrap-4.min.js"></script>
    
    <!-- Template Javascript -->
    <script src="{{ asset('ui/frontend') }}/js/main.js"></script>