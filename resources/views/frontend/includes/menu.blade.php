<div class="container-xxl py-5">
    <div class="container">
        <div class="text-center wow fadeInUp" data-wow-delay="0.1s">
            <h5 class="section-title ff-secondary text-center text-primary fw-normal">Category Food </h5>
            <h1 class="mb-5">Most Popular Category</h1>
        </div>
        <div class="tab-class text-center wow fadeInUp" data-wow-delay="0.1s">
           
        {{-- here category --}}
        <div class="row">
            @foreach ($categories as $category)
            <div class="col-lg-3">
                <div class="card shadow" style="width: 16rem;">
                    <a href="{{route('front_categoryWiseItem', $category->id)}}">
                        <img src="{{ asset('storage/categories/'. $category->image) }}" class="card-img-top" alt="...">
                        <div class="card-body">   
                            <p class="card-text">{{$category->description ?? ''}}</p>
                            <h3 class="card-title d-flex"><i class="fa fa-coffee fa-2x text-primary"></i> {{$category->name ?? ''}}</h3>
                          </div>
                    </a>
                </div>
            </div>
            @endforeach
        </div>

       
        {{-- Item here --}}
        <br>
        <div class="container-xxl py-5">
            <div class="container">
                <div class="text-center wow fadeInUp" data-wow-delay="0.1s">
                    <h5 class="section-title ff-secondary text-center text-primary fw-normal">Food Menu</h5>
                    <h1 class="mb-5">Most Popular Items</h1>
                </div> 
       
           <div id="tab-1" class="">
                <div class="row">
                        @foreach ($items as $item)
                        
                            <div class="col-lg-3 mt-4 ">
                                <form action="{{route('addcart', $item->id)}}" method="POST">
                                    @csrf
                                <div class="card shadow" style="width: 16rem , hight:100rem;">
                                    <img  src="{{ asset('storage/items/'. $item->image) }}" class="card-img-top" alt="">
                                    <div class="card-body">
                                        <h3 class="card-title">{{$item->name ?? ''}}</h3>
                                        <p class="card-text">{{$item->description ?? ''}}</p>
                                        <input type="text" name="price" value="{{$item->price}} " hidden>
                                        <span class="text-primary">Price <del>{{$item->price + 50 ?? ''}}</del> {{$item->price}}BDT</span> <br>
                                        <span class="text-primary">Discount {{$item->discount ?? ''}}</span> <br>
                                        <a href="{{route('item_details', $item->id)}}" class="btn btn-primary">See details</a>
                                    </div>
                                </div>
                                <br>
                                <input type="number" name="quantity" min="1" value="1" style="width: 100px; ">
                                <input type="submit"  value="add cart">
                            </div>
                        </form>
                        @endforeach
                </div>
            </div>
        
        </div>
    </div>
</div>
</div>