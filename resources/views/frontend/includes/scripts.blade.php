<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"></script>
<script src="{{ asset('ui/frontend') }}/lib/wow/wow.min.js"></script>
<script src="{{ asset('ui/frontend') }}/lib/easing/easing.min.js"></script>
<script src="{{ asset('ui/frontend') }}/lib/waypoints/waypoints.min.js"></script>
<script src="{{ asset('ui/frontend') }}/lib/counterup/counterup.min.js"></script>
<script src="{{ asset('ui/frontend') }}/lib/owlcarousel/owl.carousel.min.js"></script>
<script src="{{ asset('ui/frontend') }}/lib/tempusdominus/js/moment.min.js"></script>
<script src="{{ asset('ui/frontend') }}/lib/tempusdominus/js/moment-timezone.min.js"></script>
<script src="{{ asset('ui/frontend') }}/lib/tempusdominus/js/tempusdominus-bootstrap-4.min.js"></script>

<!-- Template Javascript -->
<script src="{{ asset('ui/frontend') }}/js/main.js"></script>