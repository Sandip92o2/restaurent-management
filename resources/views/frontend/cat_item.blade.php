@extends('frontend.master')

@section('content')

      <!-- Menu Start -->
      <br>
      <br>
      <div class="container-xxl py-5">
        <div class="container">
            <div class="text-center wow fadeInUp" data-wow-delay="0.1s">
                <h5 class="section-title ff-secondary text-center text-primary fw-normal">Food Item</h5>
                <h1 class="mb-5">Most Popular Items</h1>
            </div>
            <div class="tab-class text-center wow fadeInUp" data-wow-delay="0.1s">
              
                <div class="tab-content">
                   
                    <div id="tab-1" class="tab-pane fade show p-0 active">
                    
                        <div class="row">
                            @foreach ($category->items as $item)
                            <form action="{{route('addcart', $item->id)}}" method="POST">
                                @csrf
                                <div class="col-lg-3">
                                    <div class="card shadow" style="width: 16rem , hight:100rem;">
                                        <img src="{{ asset('storage/items/'. $item->image) }}" class="card-img-top" alt="">
                                        <div class="card-body">
                                            <h3 class="card-title">{{$item->name ?? ''}}</h3>
                                            <p class="card-text">{{$item->description ?? ''}}</p>
                                            <span class="text-primary">Price <del>{{$item->price + 50 ?? ''}}</del> {{$item->price}}BDT</span> <br>
                                           <span class="text-primary">Discount {{$item->discount ?? ''}}</span> <br>
                                            <a href="#" class="btn btn-primary">See details</a>
                                        </div>
                                    </div>
                                    <input type="number" name="quantity" min="1" value="1" style="width: 100px; ">
                                    <input type="submit"  value="add cart">
                                </div>
                            </form>
                            @endforeach
                        </div>
                    
                    </div>   
                
                </div>
            </div>
        </div>
    </div>
    <!-- Menu End -->
@endsection